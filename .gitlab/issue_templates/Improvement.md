### Proposal

(The suggested improvement, the feature(s) it relates to, etc.)

### Shortcoming

(Why is the current behaviour inadequate?)

### Improvement Checklist

Make sure these are completed before closing the issue,
with a link to the relevant commit.

- [ ] Code written
- [ ] Tests updated
- [ ] Documentation updated
- [ ] Changes analyzed by detekt

/label ~improvement

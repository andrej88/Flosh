### Proposal

(The proposed feature, how it should be used, where it should be located, etc.)

### Benefits

(How will the feature improve the user experience?)

### Feature Checklist

Make sure these are completed before closing the issue,
with a link to the relevant commit.

- [ ] Code written
- [ ] Tests written
- [ ] Documentation written
- [ ] Changes analyzed by detekt

/label ~feature

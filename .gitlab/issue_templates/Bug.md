### Description

(Summarize the bug encountered concisely)

### Steps to Reproduce

(How one can reproduce the issue - this is very important)

### Expected Behaviour

(What should happen)

### Observed Behaviour

(What actually happens)

### Relevant logs and/or screenshots

(Paste any relevant logs - please use code blocks (```) to format console output,
logs, and code as it's very hard to read otherwise.)

### Additional Notes

(Anything else worth mentioning)

### Bug Checklist

- [ ] Code fixed
- [ ] Tests updated
- [ ] Changes analyzed by detekt

/label ~bug

### Purpose

(What the action will do.)

### Input Ports

| Name           | [Data Type](src/main/kotlin/io/flosh/app/action/DataType.kt) | Description |
|----------------|----------------|--------------------------------------|
| Example        | Text           | The text to take in                  |

### Output Ports

| Name           | [Data Type](src/main/kotlin/io/flosh/app/action/DataType.kt) | Description |
|----------------|----------------|--------------------------------------|
| Example        | Text           | The text to provide                  |

### Action Checklist

Make sure these are completed before closing the issue.

- [ ] Code written
- [ ] Tests written
- [ ] Documentation written
- [ ] Changes analyzed by detekt

/label ~feature
/label ~action

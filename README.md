[![Apache License](https://img.shields.io/badge/license-Apache%20License%202.0-blue.svg)](http://www.apache.org/licenses/LICENSE-2.0)

# Flosh

![Flosh Logo](src/main/resources/Logo/Logo 128.png)

Flosh is a desktop program for designing flowcharts representing command scripts and exporting them as bash files.

The aim is to make command scripting more accessible to those without knowledge of bash, batch, and similar languages, without sacrificing the efficiency and portability of regular script files.

Flosh is licensed under the Apache License, Version 2.0. See LICENSE for details.


## Getting Started

### Development

Flosh uses Gradle for its build system. To set up Flosh in an IDE, import it as a Gradle project. To run the program, execute Gradle's "run" task.

Flosh is based on TornadoFX, a JavaFX framework for Kotlin. If you use Intellij IDEA, the TornadoFX plugin will provide some useful features.

[Detekt](https://github.com/arturbosch/detekt) is used for static code analysis. The `detektCheck` Gradle task runs the inspection according to `detekt/main.yml`.
package io.flosh.view

import javafx.event.EventTarget
import javafx.scene.layout.Priority
import tornadofx.hgrow
import tornadofx.region

/**
 * Creates an empty region that will expand to fit all available space (hgrow = Priority.Always)
 */
fun EventTarget.expando() {
    region {
        hgrow = Priority.ALWAYS
    }
}
package io.flosh.view

import javafx.scene.effect.BlurType
import javafx.scene.effect.DropShadow
import javafx.scene.paint.Color
import tornadofx.Stylesheet
import tornadofx.box
import tornadofx.cssclass
import tornadofx.px

/**
 * TornadoFX Style definitions
 */
class Styles : Stylesheet()
{

    companion object
    {
        /**
         * Style information for buttons in the toolbar of the [MainView].
         */
        val toolbarButton by cssclass()

        /**
         * Style information for the canvas upon which nodes are placed.
         */
        val floshCanvas by cssclass()

        /**
         * Style information for [FloshNodes][io.flosh.view.FloshNode].
         */
        val floshNode by cssclass()

        /**
         * Style information for [ports][io.flosh.view.FloshNode.Port], including the label, the
         * plug circle, and the empty space opposite the plug.
         */
        val floshNodePortLabelContainer by cssclass()

        /**
         * Style information for the label portion of [ports][io.flosh.view.FloshNode.Port].
         */
        val floshNodePortLabel by cssclass()
    }

    init
    {

        button and toolbarButton {
            padding = box(Toolbar.Button.PADDING.px)
        }

        floshCanvas {
            backgroundColor += Color.LIGHTGRAY
        }

        floshNode {
            backgroundColor += FloshNode.Colors.BACKGROUND
            padding = box(FloshNode.PADDING)
            borderColor += box(FloshNode.Colors.BORDER)
            borderWidth += box(FloshNode.BORDER_WIDTH)

            effect = FloshNode.SHADOW

            borderRadius += box(FloshNode.CORNER_RADIUS)
            backgroundRadius += box(FloshNode.CORNER_RADIUS)
        }

        floshNodePortLabelContainer {
            backgroundColor += FloshNode.Port.Colors.LABEL_BACKGROUND
            borderColor += box(FloshNode.Port.Colors.LABEL_BORDER)
            borderWidth += box(FloshNode.Port.BORDER_WIDTH)
        }

        floshNodePortLabel {
            padding = box(vertical = FloshNode.Port.LABEL_PADDING_VERTICAL,
                          horizontal = FloshNode.Port.LABEL_PADDING_HORIZONTAL)
        }
    }

    /**
     * Constants for toolbar styling.
     */
    @Suppress("MagicNumber")
    object Toolbar
    {
        /**
         * Constants for [toolbarButton].
         */
        object Button
        {
            /**
             * The space between icon and button border.
             */
            const val PADDING = 4
        }
    }

    /**
     * Constants for [floshNode].
     */
    @Suppress("MagicNumber")
    object FloshNode
    {
        /**
         * The space between the text and the border of the [FloshNode][io.flosh.view.FloshNode].
         */
        val PADDING = 2.px

        /**
         * Width of [FloshNode's][io.flosh.view.FloshNode] border.
         */
        val BORDER_WIDTH = 1.px

        /**
         * Curvature of [FloshNode's][io.flosh.view.FloshNode] corners
         */
        val CORNER_RADIUS = 3.px

        private val SHADOW_COLOR = Color.BLACK.deriveColor(0.0, 1.0, 1.0, 0.5)
        private const val SHADOW_RADIUS = 6.0
        private const val SHADOW_SPREAD = 0.0
        private const val SHADOW_OFFSET_X = 1.0
        private const val SHADOW_OFFSET_Y = 1.0

        /**
         * The drop shadow to be used by [FloshNodes][io.flosh.view.FloshNode].
         */
        val SHADOW = DropShadow(BlurType.GAUSSIAN,
                                SHADOW_COLOR,
                                SHADOW_RADIUS,
                                SHADOW_SPREAD,
                                SHADOW_OFFSET_X,
                                SHADOW_OFFSET_Y)

        /**
         * Colors for [floshNode].
         */
        object Colors
        {
            /**
             * Color of [FloshNode's][io.flosh.view.FloshNode] background.
             */
            val BACKGROUND: Color = Color.TEAL.deriveColor(0.0, 1.0, 1.4, 0.7)

            /**
             * Color of [FloshNode's][io.flosh.view.FloshNode] border.
             */
            val BORDER: Color = Color.TEAL
        }

        /**
         * Constants for [floshNodePortLabelContainer] and [floshNodePortLabel].
         */
        object Port
        {
            /**
             * Width of [Port's][io.flosh.view.FloshNode.Port] border.
             */
            val BORDER_WIDTH = 1.px

            /**
             * The vertical space between the text and the border of the
             * [Port][io.flosh.view.FloshNode.Port].
             */
            val LABEL_PADDING_VERTICAL = 0.px

            /**
             * The horizontal space between the text and the border of the
             * [Port][io.flosh.view.FloshNode.Port].
             */
            val LABEL_PADDING_HORIZONTAL = 3.px

            /**
             * Colors for [floshNodePortLabelContainer] and [floshNodePortLabel].
             */
            object Colors
            {
                /**
                 * Color of [Port's][io.flosh.view.FloshNode.Port] background.
                 */
                val LABEL_BACKGROUND: Color = Color.DARKGRAY.deriveColor(0.0, 1.0, 1.0, 0.5)

                /**
                 * Color of [Port's][io.flosh.view.FloshNode.Port] border.
                 */
                val LABEL_BORDER: Color = Color.DARKGRAY
            }

            /**
             * Constants for the plug [circle][javafx.scene.shape.Circle] representing the plug of
             * a [FloshNode.Port].
             */
            object Plug
            {
                /**
                 * Width of area next to the label, which hosts either the plug or an empty space.
                 */
                const val COLUMN_WIDTH = 15.0

                /**
                 * Radius of the plug circle.
                 */
                const val RADIUS = 4.0

                private const val PADDING = 1.0

                /**
                 * Width of the plug's border.
                 */
                const val BORDER_WIDTH = 1.0

                /**
                 * Horizontal center of the plug circle.
                 */
                const val CENTER_X = RADIUS + PADDING + BORDER_WIDTH

                /**
                 * Vertical center of the plug circle.
                 */
                const val CENTER_Y = RADIUS + PADDING + BORDER_WIDTH

                /**
                 * Colors for [Plug].
                 */
                object Colors
                {
                    /**
                     * Color of the plug's border.
                     */
                    val BORDER: Color = Color.web("#202020")

                    /**
                     * Fill color of the plug when its Port is not connected to any other ports,
                     * and the cursor isn't hovering over the plug.
                     */
                    val UNCONNECTED: Color = Color.TRANSPARENT

                    /**
                     * Fill color of the plug when its Port is connected to at least one port,
                     * and the cursor isn't hovering over the plug.
                     */
                    val CONNECTED: Color = Color.web("#606060")

                    /**
                     * Fill color of the plug when its Port is not connected to any other ports,
                     * and the cursor is hovering over the plug.
                     */
                    val UNCONNECTED_HOVER: Color = CONNECTED.deriveColor(0.0, 1.0, 1.0, 0.5)

                    /**
                     * Fill color of the plug when its Port is connected to at least one port,
                     * and the cursor is hovering over the plug.
                     */
                    val CONNECTED_HOVER: Color = Color.web("#404040")
                }
            }
        }
    }
}
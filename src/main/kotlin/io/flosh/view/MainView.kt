package io.flosh.view

import io.flosh.app.action.DataType
import io.flosh.app.action.IdentityAction
import javafx.scene.control.Button
import javafx.scene.image.ImageView
import javafx.scene.layout.Priority
import tornadofx.View
import tornadofx.addClass
import tornadofx.button
import tornadofx.region
import tornadofx.separator
import tornadofx.toolbar
import tornadofx.tooltip
import tornadofx.vbox
import tornadofx.vgrow

/**
 * The view shown in the main window.
 */
class MainView : View("Flosh")
{
    companion object
    {
        const val ID_CANVAS = "canvas"
        const val ID_TOOLBAR = "toolbar"
    }

    override val root = vbox {

        toolbar {

            id = ID_TOOLBAR

            listOf(
                    button(graphic = ImageView("Icons/Fugue/new.png")) {
                        tooltip = tooltip("New")
                    },

                    button(graphic = ImageView("Icons/Fugue/open.png")) {
                        tooltip = tooltip("Open")
                    },

                    button(graphic = ImageView("Icons/Fugue/save.png")) {
                        tooltip = tooltip("Save")
                    },

                    button(graphic = ImageView("Icons/Fugue/export.png")) {
                        tooltip = tooltip("Export")
                    },

                    separator(),

                    button(graphic = ImageView("Icons/Fugue/node add.png")) {
                        tooltip = tooltip("Add Node")

                        setOnMouseClicked {
                            // TODO: This should ask the user what kind of action they'd like.
                            this@vbox.lookup("#$ID_CANVAS")
                                     .add(FloshNode(IdentityAction(DataType.TEXT)))
                        }
                    }
            ).forEach { (it as? Button)?.addClass(Styles.toolbarButton) }
        }

        region {
            id = ID_CANVAS
            addClass(Styles.floshCanvas)
            vgrow = Priority.ALWAYS
        }
    }
}
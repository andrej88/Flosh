package io.flosh.view

import io.flosh.app.action.FloshAction
import io.flosh.app.action.FloshPort
import io.flosh.util.clamp
import javafx.geometry.HPos
import javafx.geometry.Point2D
import javafx.scene.Cursor
import javafx.scene.layout.ColumnConstraints
import javafx.scene.layout.Priority
import tornadofx.View
import tornadofx.addClass
import tornadofx.circle
import tornadofx.gridpane
import tornadofx.hbox
import tornadofx.label
import tornadofx.region
import tornadofx.row
import tornadofx.vbox

/**
 * The base flowchart node (not to be confused with JavaFX scene nodes).
 *
 * @property action The [FloshAction] belonging to this node
 * @property displayName The name shown to the user, defaults to the [action]'s
 * [name][FloshAction.name].
 */
class FloshNode(val action: FloshAction,
                private val displayName: String = action.name) : View("Flosh Node")
{
    // This is necessary because sceneX on its own uses the whole scene, not the region node.
    private lateinit var offsetInNode: Point2D

    // The node lags a bit behind the actual cursor position, so several mouseEnter and mouseExit
    // events may occur while dragging. This flag can be used to prevent certain code from being
    // called under those circumstances.
    private var isDragging = false

    override val root = vbox {

        spacing = 1.0

        // Add input ports
        action.inputs.forEach { add(Port(it)) }

        // Add the main node body
        gridpane {

            addClass(Styles.floshNode)

            label(displayName)

            setOnMousePressed {
                isDragging = true
                offsetInNode = Point2D(parent.layoutX - it.sceneX,
                                       parent.layoutY - it.sceneY)
                cursor = Cursor.MOVE
            }

            setOnMouseDragged {
                val positionInRegionX = it.sceneX + offsetInNode.x
                val positionInRegionY = it.sceneY + offsetInNode.y

                val regionBounds = parent.parent.boundsInLocal
                val newX = positionInRegionX.clamp(regionBounds.minX, regionBounds.maxX - width)
                val newY = positionInRegionY.clamp(regionBounds.minY, regionBounds.maxY - height)

                // TODO: Expand window -> move node to edge -> shrink window -> node is now hidden
                parent.relocate(newX, newY)
            }

            setOnMouseReleased {
                isDragging = false
                cursor = Cursor.HAND
            }

            setOnMouseEntered {
                if (!isDragging)
                {
                    cursor = Cursor.HAND
                }
            }

            setOnMouseExited {
                if (!isDragging)
                {
                    cursor = Cursor.DEFAULT
                }
            }
        }


        // Add the output ports
        action.outputs.forEach { add(Port(it)) }
    }

    /**
     * The rendered port, drawn next to a [FloshNode]. Consists of the box
     * (containing a label) and a "plug" on the side, which is how the port
     * visually connects to other ports.
     *
     * @property port The [FloshPort] belonging to this port
     * @property displayName The name shown to the user, defaults to the [port]'s
     * [name][FloshPort.name].
     */
    private class Port(val port: FloshPort,
                       val displayName: String = port.name) : View("Flosh Node Port")
    {
        override val root = gridpane {

            val col1constraints = ColumnConstraints().apply {
                maxWidth = Styles.FloshNode.Port.Plug.COLUMN_WIDTH
                minWidth = maxWidth
                halignment = HPos.CENTER
            }

            val col2constraints = ColumnConstraints().apply {
                hgrow = Priority.ALWAYS
            }

            val col3constraints = ColumnConstraints().apply {
                maxWidth = Styles.FloshNode.Port.Plug.COLUMN_WIDTH
                minWidth = maxWidth
                halignment = HPos.CENTER
            }
            columnConstraints.addAll(col1constraints, col2constraints, col3constraints)

            row {

                val portPlug = circle(centerX = Styles.FloshNode.Port.Plug.CENTER_X,
                                      centerY = Styles.FloshNode.Port.Plug.CENTER_Y,
                                      radius = Styles.FloshNode.Port.Plug.RADIUS) {

                    stroke = Styles.FloshNode.Port.Plug.Colors.BORDER
                    strokeWidth = Styles.FloshNode.Port.Plug.BORDER_WIDTH
                    fill = Styles.FloshNode.Port.Plug.Colors.UNCONNECTED


                    setOnMouseEntered {

                        cursor = Cursor.HAND

                        fill = if (!port.hasConnection)
                        {
                            Styles.FloshNode.Port.Plug.Colors.UNCONNECTED_HOVER
                        }
                        else
                        {
                            Styles.FloshNode.Port.Plug.Colors.CONNECTED_HOVER
                        }
                    }

                    setOnMouseExited {

                        cursor = Cursor.DEFAULT

                        fill = if (!port.hasConnection)
                        {
                            Styles.FloshNode.Port.Plug.Colors.UNCONNECTED
                        }
                        else
                        {
                            Styles.FloshNode.Port.Plug.Colors.CONNECTED
                        }
                    }
                }

                val portBox = hbox {
                    addClass(Styles.floshNodePortLabelContainer)

                    if (port is FloshPort.OutputPort)
                    {
                        expando()
                    }

                    label(displayName) {
                        addClass(Styles.floshNodePortLabel)
                    }

                    if (port is FloshPort.InputPort)
                    {
                        expando()
                    }
                }

                if (port is FloshPort.InputPort)
                {
                    addColumn(0, portPlug)
                }
                else
                {
                    addColumn(0, region())
                }

                addColumn(1, portBox)

                if (port is FloshPort.OutputPort)
                {
                    addColumn(2, portPlug)
                }
                else
                {
                    addColumn(2, region())
                }
            }
        }
    }
}

package io.flosh.util

/**
 * Returns the receiver clamped to the range `lowerBound..upperBound`
 *
 * If receiver < [lowerBound], returns [lowerBound].
 *
 * If receiver > [upperBound], returns [upperBound].
 *
 * Otherwise, returns receiver.
 *
 * @receiver The value to clamp.
 * @param T a [Comparable] value
 * @param lowerBound low end of the range to which the receiver is clamped
 * @param upperBound high end of the range to which the receiver is clamped
 * @return The clamped value. Always returns one of receiver, lowerBound, or upperBound.
 *
 * @sample
 *
 */
fun <T : Comparable<T>> T.clamp(lowerBound: T, upperBound: T) =
        when
        {
            this < lowerBound -> lowerBound
            this > upperBound -> upperBound
            else -> this
        }
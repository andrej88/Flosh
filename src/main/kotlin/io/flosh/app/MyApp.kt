package io.flosh.app

import io.flosh.view.MainView
import io.flosh.view.Styles
import javafx.scene.image.Image
import javafx.stage.Stage
import tornadofx.App
import tornadofx.addStageIcon

/**
 * The main class run when the program is first launched.
 */
class MyApp: App(MainView::class, Styles::class) {

    init {
        addStageIcon(Image("Logo/Logo 16 outlined.png"), scope)
        addStageIcon(Image("Logo/Logo 32 outlined.png"), scope)
    }

    override fun start(stage: Stage)
    {
        super.start(stage)

        stage.minWidth = WINDOW_HEIGHT
        stage.minHeight = WINDOW_WIDTH

        stage.centerOnScreen()
    }

    companion object {
        const val WINDOW_HEIGHT = 800.0
        const val WINDOW_WIDTH = 600.0
    }
}
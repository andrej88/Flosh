package io.flosh.app

/**
 * To be thrown when a serious error is encountered that indicates a problem in
 * the code.
 */
class FloshError(message: String = "", cause: Throwable? = null) : Error(message, cause)
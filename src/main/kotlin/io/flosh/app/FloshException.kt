package io.flosh.app

/**
 * A generic exception to be thrown by classes in the app package. These exceptions should be
 * caught and displayed as a message to the user.
 */
open class FloshException(message: String = "", cause: Throwable? = null) :
        Exception(message, cause)
package io.flosh.app.action

import io.flosh.app.FloshError

/**
 * A wrapper around a List<[T]>.
 *
 * @param T Type of [FloshPort] ([input][FloshPort.InputPort] or [output][FloshPort.OutputPort])
 * &nbsp;this list contains
 */
class PortList<out T : FloshPort>(vararg ports: T) : Iterable<T>
{
    private val ports: List<T> = ports.toList()

    val areDataReady: Boolean
        get() = ports.all { it.data != null }

    /**
     * Gets the port by [name]
     *
     * @throws FloshError if no port exists with the given name.
     */
    operator fun get(name: String) = ports.find { it.name == name } ?:
                                     throw FloshError("Port \"$name\" not found!")

    override fun iterator(): Iterator<T> = ports.iterator()
}
package io.flosh.app.action

/**
 * Strings can represent various different types of data. Usually it is
 * desirable to make sure the right kind of data is going to the right places
 * (e.g. you don't want a list of files to be used instead of a regular
 * expression), but if the user knows what they're doing they can decide to
 * ignore this.
 */
enum class DataType
{
    /**
     * Generic text without any specified meaning.
     */
    TEXT,

    /**
     * A string representing a list of files.
     */
    FILE_LIST,

    /**
     * A regular expression pattern.
     */
    REGEX_PATTERN
}
package io.flosh.app.action

import io.flosh.app.FloshException
import io.flosh.app.action.FloshPort.InputPort
import io.flosh.app.action.FloshPort.OutputPort

/**
 * Instances of [InputPort] and [OutputPort] are the inputs and outputs of the
 * [owner].
 *
 * @property name Name of the port used internally.
 * @property type The [type of data][DataType] handled by the port.
 * @property owner The [FloshAction] to which the port belongs.
 */
sealed class FloshPort(val name: String,
                       val type: DataType,
                       val owner: FloshAction)
{
    /**
     * True if the port is connected to any other ports.
     */
    abstract val hasConnection: Boolean

    /**
     * The data this port transfers. The data of an input port matches that of
     * its source, and vice versa. `Null` means there is no data, and an empty
     * string means the data is there, but it's empty.
     */
    open var data: String? = null

    /**
     * Instances of this class are the inputs of the action.
     */
    class InputPort(name: String, type: DataType, owner: FloshAction) : FloshPort(name, type, owner)
    {
        /**
         * The [OutputPort] from which this port receives data.
         */
        var source: OutputPort? = null

        /**
         * Setting the data of an [InputPort] will attempt to
         * [run][FloshAction.run] the [FloshAction] to which it belongs.
         */
        override var data: String?
            get() = super.data
            set(value)
            {
                super.data = value
                owner.run()
            }

        override val hasConnection: Boolean
            get() = source != null

        /**
         * Connects this port to [target], deleting the existing connection if
         * applicable.
         *
         * @throws FloshException if [target] has a different [type][DataType].
         *
         * @see OutputPort.connectTo
         */
        fun connectTo(target: OutputPort)
        {
            source?.destinations?.remove(this)
            source = target
            target.destinations.add(this)

            if (target.type != type)
            {
                throw FloshException("Conflicting data types: " +
                                     "port \"${this.name}\" has type \"${this.type}\", " +
                                     "port \"${target.name}\" has type \"${target.type}\"")

            }
        }
    }

    /**
     * Instances of this class are the outputs of the action.
     */
    class OutputPort(name: String, type: DataType, owner: FloshAction) :
            FloshPort(name, type, owner)
    {
        /**
         * The [InputPorts][InputPort] to which this port outputs.
         */
        val destinations: MutableList<InputPort> = mutableListOf()

        /**
         * Setting the data of an output port will also set the data of the
         * destination input ports.
         *
         * @see FloshPort.data
         */
        override var data: String?
            get() = super.data
            set(value)
            {
                super.data = data
                destinations.forEach {
                    it.data = value
                }
            }

        override val hasConnection: Boolean
            get() = !destinations.isEmpty()

        /**
         * Connects this port to [target], deleting [target]'s existing
         * connection if applicable.
         *
         * @see InputPort.connectTo
         */
        fun connectTo(target: InputPort)
        {
            target.connectTo(this)
        }
    }
}
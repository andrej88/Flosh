package io.flosh.app.action

/**
 * Has one input and one output. Outputs exactly what it is given as input.
 */
class IdentityAction(type: DataType) : FloshAction("Identity")
{
    override val inputs = PortList(FloshPort.InputPort(INPUT_PORT_NAME, type, this))

    override val outputs = PortList(FloshPort.OutputPort(OUTPUT_PORT_NAME, type, this))

    override val config = Configuration.emptyConfig

    override fun executeAction()
    {
        outputs[OUTPUT_PORT_NAME].data = inputs[INPUT_PORT_NAME].data
    }

    companion object
    {
        const val INPUT_PORT_NAME = "In"
        const val OUTPUT_PORT_NAME = "Out"
    }
}

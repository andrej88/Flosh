package io.flosh.app.action

/**
 * The interface for all actions and behaviors that nodes can have.
 *
 * @property name Name of the action used internally
 */
abstract class FloshAction(val name: String)
{
    /**
     * The input ports.
     */
    open val inputs: PortList<FloshPort.InputPort> = PortList()

    /**
     * The output ports.
     */
    open val outputs: PortList<FloshPort.OutputPort> = PortList()

    /**
     * Specifies the behavior of this instance of the action
     */
    open val config: Configuration = Configuration.emptyConfig

    /**
     * Runs the action:
     * 1) Checks to see if there is data in the input ports.
     * 2) Executes the action.
     * 3) Writes the result(s) to the output ports.
     * 4) Attempts to run the next actions.
     *
     * @see FloshPort.InputPort.data
     * @see FloshPort.OutputPort.data
     */
    fun run()
    {
        if (inputs.areDataReady)
        {
            executeAction()
        }
    }

    /**
     * Executes the action using the [inputs] and rules in the [config], then
     * sets the [outputs]' data accordingly.
     */
    protected abstract fun executeAction()

    /**
     * This interface can be used to store action-specific settings and parameters
     */
    interface Configuration
    {
        companion object
        {
            val emptyConfig: Configuration
                @Suppress("EmptyClassBlock") // Omitting the braces causes a compiler error
                get() = object : Configuration {}
        }
    }
}
package io.flosh.app.action

/**
 * Does not have any inputs or outputs, no behavior.
 */
class EmptyAction : FloshAction("Empty")
{
    override fun executeAction()
    {
        // no op
    }
}

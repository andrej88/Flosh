package io.flosh.app.action

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.*
import kotlin.test.*

class FloshActionTest :
        Spek({
                 given("an action") {

                     val action: FloshAction = IdentityAction(type = DataType.TEXT)

                     on("creation") {
                         it("should have no connections") {
                             val noInputs = action.inputs.all { it.source == null }
                             val noOutputs = action.outputs.all { it.destinations.isEmpty() }
                             assertTrue(noInputs && noOutputs)
                         }
                     }
                 }

                 given("two connected actions, with data in the input of the first") {
                     val action1: FloshAction = IdentityAction(DataType.TEXT)
                     val action2: FloshAction = IdentityAction(DataType.TEXT)
                     val testData = "Lorem ipsum dolor sit amet"

                     action1.inputs[IdentityAction.INPUT_PORT_NAME].data = testData
                     action1.outputs[IdentityAction.OUTPUT_PORT_NAME]
                             .connectTo(action2.inputs[IdentityAction.INPUT_PORT_NAME])

                     on("calling run() on the first action") {
                         action1.run()
                         it("should contain data in the second action's input") {
                             assertEquals(expected = testData,
                                          actual = action2.inputs[IdentityAction.INPUT_PORT_NAME].data)
                         }
                     }
                 }
             })
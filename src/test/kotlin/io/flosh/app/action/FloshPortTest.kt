package io.flosh.app.action

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.*
import kotlin.test.assertTrue

class FloshPortTest :
        Spek({
            given("two actions with one input port and one output port each") {
                val action1 = IdentityAction(type = DataType.TEXT)
                val action2 = IdentityAction(type = DataType.TEXT)

                on("connecting") {

                    action1.outputs[IdentityAction.OUTPUT_PORT_NAME]
                            .connectTo(action2.inputs[IdentityAction.INPUT_PORT_NAME])

                    it("should have a reference to the destination action") {
                        assertTrue(action1.outputs[IdentityAction.OUTPUT_PORT_NAME].destinations.any {
                            it.owner == action2
                        })
                    }

                    it("should have a reference to the source") {
                        assertTrue(action2.inputs[IdentityAction.INPUT_PORT_NAME].source?.owner == action1)
                    }
                }
            }
     })
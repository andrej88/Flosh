package io.flosh.util

import org.jetbrains.spek.api.Spek
import org.jetbrains.spek.api.dsl.*
import kotlin.test.assertEquals

class MathUtilsTest :
        Spek({
                 given("a number") {

                     val num = 5.2

                     on("clamping to a range in which it is contained") {

                         val clamped = num.clamp(5.0, 6.0)

                         it("should return itself") {
                             assertEquals(expected = num, actual = clamped)
                         }
                     }

                     on("clamping to a range greater than itself") {

                         val lowerBound = 6.0
                         val clamped = num.clamp(lowerBound, 10.0)

                         it("should return the lower bound of that range") {
                             assertEquals(expected = lowerBound, actual = clamped)
                         }
                     }

                     on("clamping to a range lower than itself") {

                         val upperBound = 5.0
                         val clamped = num.clamp(2.0, upperBound)

                         it("should return the upper bound of that range") {
                             assertEquals(expected = upperBound, actual = clamped)
                         }
                     }
                 }
             })